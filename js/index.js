//1 Знайти всі параграфи на сторінці та встановити колір фону #ff0000

//2 Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

//3 Встановіть в якості контента елемента з класом testParagraph наступний параграф -

// This is a paragraph

//4 Отримати елементи , вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
// Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

//1

const allParagraphs = document.querySelectorAll(`p`);
// console.log(allParagraphs)

allParagraphs.forEach((item)=> {
   item.style.color = " #ff0000";
})

//2

const optionsList = document.querySelector('#optionsList');
console.log(optionsList)

console.log(optionsList.parentElement)

for (let i = 0 ; i < optionsList.childNodes.length ; i ++) {
  console.log(optionsList.childNodes[i])
}
// 3
const testParagraph = document.querySelector('#testParagraph');
console.log(testParagraph)

  testParagraph.innerHTML = 'This is a paragraph'

//4

const mainHeader = document.querySelector('.main-header');
const mainHeaderChildren = Array.from(mainHeader.children);
console.log(mainHeaderChildren)



mainHeaderChildren.forEach((item)=> {
  item.classList.add('nav-item')
})

const allTitles = document.querySelectorAll('.section-title');
// console.log(getAllTitles)
allTitles.forEach((item)=> {
  item.classList.remove('section-title')
})
console.log(allTitles)